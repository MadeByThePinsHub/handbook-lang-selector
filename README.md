| Repository | Branch | CI Status |
| ---------- | ------ | --------- |
| [Handbook Language Selector](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook-redirects) | `master` | [![pipeline status](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook-redirects/badges/master/pipeline.svg)](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook-redirects/commits/master) |
| [English Handbook (Production)](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook) | `master` | [![pipeline status](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook/badges/master/pipeline.svg)](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook/commits/master)
| [English Handbook (Staging)](https;//gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook/tree/staging) | `staging` | [![pipeline status](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook/badges/staging/pipeline.svg)](https://gitlab.com/MadeByThePinsTeam-DevLabs/official-handbook/commits/staging)

# Handbook Language Selector

## How This Works?
When someone opens `handbooksbythepins.gq` or `www.handbooksbythepins.gq`, an language selector pops out. Currently, only the **English** langauge is available at the moment, but feel free to fork and translate the handbook for your language.

## Files
* `README.md` - This file you're reading on!
* `.gitlab-ci.yml` - GitLab CI configuration file
* `public/*` - Website files, mostly HTML stuff.
